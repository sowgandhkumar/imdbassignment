﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public List<Actor> actors { get; set; }
        public String producer { get; set; }
        public Movie(string name, int date, string plot, List<Actor> actors, String producer)
        {
            this.Title = name;
            this.Year = date;
            this.Plot = plot;
            this.actors = actors;
            this.producer = producer;
        }
    }
}
