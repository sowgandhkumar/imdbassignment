﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Producer
    {
        public string Name { set; get; }
        public string DateOfBirth { set; get; }
        public Producer(String name, String dateofbirth)
        {
            this.Name = name;
            this.DateOfBirth = dateofbirth;
        }
    }
}
