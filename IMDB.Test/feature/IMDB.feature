﻿Feature: ConsoleIMDBAPP

@addmovies
Scenario: Adding a movie to the imdb
	Given I have a Movie with name "Harry Potter"
	And year is "2000"
	And plot is "i am lucky"
	And actor is "1 2"
	And producer is "gaya"
	When I add the movie the IMDB
	Then my imdb would look like this
	| Movie        | year | plot       | Actors    | Producers |
	| Harry Potter | 2000 | i am lucky | sai,sow   | ravi      |
	| HP2          | 2001 | its worst  | gaya      | raj       |


@listMovies
Scenario: List all movies in IMDB
	Given I have a IMDB with movies
	When I fetch my movie
	Then I should have the following movies
	| Movie        | year | plot       | Actors    | Producers |
	| Harry Potter | 2000 | i am lucky | sai,sow   | ravi      |
	| HP2          | 2001 | its worst  | gaya      | raj       |