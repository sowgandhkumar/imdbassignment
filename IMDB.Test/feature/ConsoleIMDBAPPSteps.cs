﻿using System;
using TechTalk.SpecFlow;
using IMDB;
using IMDB.Domain;
using IMDB.Repository;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;
using System.Linq;
using NUnit.Framework;

namespace IMDB.Test.feature
{
    [Binding]
    public class ConsoleIMDBAPPSteps
    {
        private IMDBService _IMDBService;
        private string _name, _plot,_producer;
        private List<Actor> _actor=new List<Actor>();
        private int _year;
        
        

        private Exception _exception;
        private List<Movie> _movies;

        public ConsoleIMDBAPPSteps()
        {
             _IMDBService = new IMDBService();
        }


        [Given(@"I have a Movie with name ""(.*)""")]
        public void GivenIHaveAMovieWithName(string name)
        {
            _name = name;
        }

        [Given(@"year is ""(.*)""")]
        public void GivenYearIs(int year)
        {
            _year = year;
        }

        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }


        [Given(@"actor is ""(.*)""")]
        public void GivenActorIs(string actor)
        {
            var s = actor.Split();
           
            foreach (var n in s)
            {
                _actor.Add(_IMDBService.GetActors()[Convert.ToInt32(n) - 1]);
            }
            
            
        }
        
        [Given(@"producer is ""(.*)""")]
        public void GivenProducerIs(string producer)
        {
            _producer = producer;
        }
        
       
        
        [When(@"I add the movie the IMDB")]
        public void WhenIAddTheMovieTheIMDB()
        {
            try
            {
                _IMDBService.AddMovie(_name, _year, _plot, _actor, _producer);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }
        
        
        [Then(@"my imdb would look like this")]
        public void ThenMyImdbWouldLookLikeThis(Table table)
        {

            var movieTable = table.CreateSet<(string name, int year, string plot, List<string> actors, string producer)>();
            var availablelist = _IMDBService.GetMovies();
            int movieindex = 0;
            foreach( var i in movieTable)
            {

                int index = 0;
                var localMovieslist = availablelist[movieindex];
                Assert.AreEqual(i.name, localMovieslist.Title);
                Assert.AreEqual(i.year, localMovieslist.Year);
                Assert.AreEqual(i.plot, localMovieslist.Plot);

                foreach(var actor in localMovieslist.actors)
                {
                    Assert.AreEqual(i.actors[index], actor.Name);
                    index++;
                }
                movieindex++;


            }
        }

        [Given(@"I have a IMDB with movies")]
        public void GivenIHaveAIMDBWithMovies()
        {

        }

        [When(@"I fetch my movie")]
        public void WhenIFetchMyMovie()
        {
            _movies = _IMDBService.GetMovies();
        }

        [Then(@"I should have the following movies")]
        public void ThenIShouldHaveTheFollowingMovies(Table table)
        {
            var movieTable = table.CreateSet<(string name, int year, string plot, List<string> actors, string producer)>();
            var availablelist = _IMDBService.GetMovies();
            int movieindex = 0;
            foreach (var i in movieTable)
            {

                int index = 0;
                var localMovieslist = availablelist[movieindex];
                Assert.AreEqual(i.name, localMovieslist.Title);
                Assert.AreEqual(i.year, localMovieslist.Year);
                Assert.AreEqual(i.plot, localMovieslist.Plot);

                foreach (var actor in localMovieslist.actors)
                {
                    Assert.AreEqual(i.actors[index], actor.Name);
                    index++;
                }
                movieindex++;


            }


        }

        [BeforeScenario("addmovies")]
        public void AddSampleMovieForAdd()
        {
            var p = new Actor("sai", "200");
            var q = new Actor("sow", "200");
            _IMDBService.AddMovie("Harry Potter", 2000, "i am lucky", new List<Actor>() { p,q }, "ravi");
            _IMDBService.AddActor("sai","200");
            _IMDBService.AddActor("sow", "200");



            var k= new Actor("gaya", "200");
            _IMDBService.AddMovie("HP2", 2001, "its worst", new List<Actor>() { k }, "raj");



        }

        [BeforeScenario("listMovies")]
        public void AddSampleMovieForList()
        {
            var p = new Actor("sai", "200");
            var q = new Actor("sow", "200");
            _IMDBService.AddMovie("Harry Potter", 2000, "i am lucky", new List<Actor>() { p, q }, "ravi");
            _IMDBService.AddActor("sai", "200");
            _IMDBService.AddActor("sow", "200");

            var k = new Actor("gaya", "200");
            _IMDBService.AddMovie("HP2", 2001, "its worst", new List<Actor>() { k }, "raj");

        }










    }
}