﻿using IMDB.Domain;
using System;
using System.Collections.Generic;

namespace IMDB
{
    public class Program
    {
        private const string Value = "1. List Movies\n2. Add Movie\n3. Add Actor\n4. Add Producer\n5. Delete Movie\n6. Exit";

        static void Main(string[] args)
        {
            int c;
            IMDBService service = new IMDBService();
            while (true)
            {
                Console.WriteLine(Value);
                int choise = Convert.ToInt32(Console.ReadLine());
                switch (choise)
                {
                    case 1:
                        foreach (var m in service.GetMovies())
                        {
                            Console.WriteLine("{0} ({1})", m.Title, m.Year);
                            Console.WriteLine("plot - {0}", m.Plot);
                            Console.Write("Actors - ");
                            foreach (var a in m.actors)
                            {
                                Console.Write("{0} ", a);
                            }
                            Console.WriteLine("\nProducer - {0}", m.producer);
                        }
                        break;
                    case 2:
                        Console.Write("Name: ");
                        String name = Console.ReadLine();
                        Console.Write("Year of Release: ");

                        int year = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Plot: ");
                        String plot = Console.ReadLine();

                        Console.WriteLine("Choose actor(s):");
                        c = 1;
                        foreach (var actor in service.GetActors())
                        {
                            Console.Write(c);
                            Console.Write(".");
                            Console.WriteLine(actor.Name);
                            c++;
                        }
                        String[] option = Console.ReadLine().Split(" ");
                        List<Actor> actors = new List<Actor>();
                        foreach (var n in option)
                        {
                            actors.Add(service.GetActors()[Convert.ToInt32(n) - 1]);
                        }

                        String producer;
                        Console.WriteLine("Choose Producer: ");
                        c = 1;
                        foreach (var p in service.GetProducer())
                        {
                            Console.Write(c);
                            Console.Write(".");
                            Console.WriteLine(p.Name);
                            c++;
                        }
                        int opt = Convert.ToInt32(Console.ReadLine()) - 1;
                        producer = service.GetProducer()[opt].Name;
                        service.AddMovie(name, year, plot, actors, producer);
                        break;
                    case 3:
                        Console.Write("Name: ");
                        String actorname = Console.ReadLine();
                        Console.Write("DOB: ");
                        String actorDOB = Console.ReadLine();
                        service.AddActor(actorname, actorDOB);
                        break;
                    case 4:
                        Console.Write("Name: ");
                        String producerName = Console.ReadLine();
                        Console.Write("DOB: ");
                        String producerDOB = Console.ReadLine();
                        service.AddProducer(producerName, producerDOB);
                        break;
                    case 5:
                        Console.Write("Name: ");
                        String movieName = Console.ReadLine();
                        Console.Write("Year: ");
                        int releaseYear = Convert.ToInt32(Console.ReadLine());
                        service.DeleteMovie(movieName, releaseYear);
                        break;
                    case 6:
                        System.Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Enter Correct option");
                        break;
                }
            }
        }
    }
}