﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;

namespace IMDB
{
     public class IMDBService
    {
        public MovieRepository _rep;
        public ActorRepository _repA;
        public ProducerRepository _repP;
        public IMDBService()
        {
            _rep = new MovieRepository();
            _repA = new ActorRepository();
            _repP = new ProducerRepository();
        }

        public void AddMovie(string name, int dateofrelease, string plot, List<Actor> actor, String producer)
        {
            var movie = new Movie(name, dateofrelease, plot, actor, producer);
            _rep.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _rep.Get();
        }
        public void AddActor(String name, string dateofbirth)
        {
            var actor = new Actor(name, dateofbirth);
            _repA.Add(actor);
        }
        public List<Actor> GetActors()
        {
            return _repA.Get();
        }
        public void AddProducer(String name, string dateofbirth)
        {
            var producer = new Producer(name, dateofbirth);
            _repP.Add(producer);
        }
        public List<Producer> GetProducer()
        {
            return _repP.Get();
        }

        public void DeleteMovie(String name,int yearofrelease)
        {
            foreach (Movie m in GetMovies())
            {
                if (m.Title.Equals(name, StringComparison.InvariantCultureIgnoreCase) && m.Year == yearofrelease)
                {
                    _rep._movies.Remove(m);
                }
            }
        }





    }
}
