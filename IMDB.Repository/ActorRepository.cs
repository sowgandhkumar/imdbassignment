﻿using IMDB.Domain;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Repository
{
    public class ActorRepository
    {
        private List<Actor> _Actors;
        public ActorRepository()
        {
            _Actors = new List<Actor>();
        }
        public void Add(Actor a)
        {
            _Actors.Add(a);
        }
        public List<Actor> Get()
        {
            return _Actors.ToList();
        }
    }
}
