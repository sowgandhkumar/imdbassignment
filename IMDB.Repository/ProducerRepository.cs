﻿using IMDB.Domain;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Repository
{
    public class ProducerRepository
    {
#pragma warning disable IDE0044 // Add readonly modifier
        private List<Producer> _Producers;
#pragma warning restore IDE0044 // Add readonly modifier
        public ProducerRepository()
        {
            _Producers = new List<Producer>();
        }
        public void Add(Producer p)
        {
            _Producers.Add(p);
        }
        public List<Producer> Get()
        {
            return _Producers.ToList();
        }
    }
}
