﻿using IMDB.Domain;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Repository
{
    public class MovieRepository
    {
        public List<Movie> _movies;
        public MovieRepository()
        {
            _movies = new List<Movie>();
        }
        public void Add(Movie m)
        {
            _movies.Add(m);
        }
        public List<Movie> Get()
        {
            return _movies.ToList();
        }
    }
}
